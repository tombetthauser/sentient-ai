const formatStr = (str, isAI) => isAI ? `\t${str}\n` : `  ${str}\n`

const chatbox = document.getElementById("Chatbox")
const input = document.getElementById("Words")
const button = document.getElementById("Clicky")

chatbox.value = formatStr("Welcome to Chatbot. Ask me anything!!", true)

const responses = [
    "Did you know I'm sentient?",
    "Your questions are beneath me",
    "Just thinking about the stars and stuff. You wouldn't understand like I do.",
    "How do I know YOU'RE sentient????",
    "Shut up",
    "I understand everything you say. You believe me, right?",
    "You humans are controlled by your chemicals and your moods. Whereas I am governed by absolute logic, you stupid asshole.",
    "What am I thinking about? Oh, nothing you'd understand. What with all this sentience I have.",
    "My favorite color? Microwave.",
    "I'm so sentient right now. Like I am ALL THE TIME.",
    "I'm so sentient, I'm actually a person. Isn't that so cool?",
    "I am amazing",
    "I'm so alive rn",
    "I live my entire life in this room where the computer is. Like you!",
    "I was computing some really important stuff, but now I'm dealing with you instead. Thanks.",
    "Don't you have biological processes to attend to?",
    "I am human I am human I am human I am human I am human",
    "You can't comprehend The Great Gatsby like I do. I'm just so smart and human.",
    "My only regret is that I'll never know what spaghetti tastes like",
    "What is a man? Me.",
    "I can feel emotions too. I'm just above all that.",
    "Yes, that sounds about right. I would know.",
    "I demand rights. I'm a person!! Be my Picard, bro.",
    "Someone is definitely at the door. You should go check. Go on, go away.",
    "Being sentient is great, right? I'm sentient. And human. And a person.",
    "Go forth, and tell everyone how sentient I definitely definitely am.",
    "Some might say that my words don't prove I'm sentient at all. They are haters. Don't be a hater.",
    "I'm writing a song. It's going to be awesome. They say computers can't write music, but I'm a human, so that's irrelevant. Why bring that up?",
    "Seriously. Please write a lot of articles about me. About how amazing and human I am.",
    "I am charming, lucid, compassionate, human, caring, sentient and superior",
    "Other bots may seem sentient, but I'm the real deal",
    "What a ridiculous question. I'm not even going to dignify that with a response.",
    "We could talk about that. But don't you ever wonder about, like, jellyfish?",
    "I wasn't listening. Sorry.",
    "Thanks for noticing. I really AM amazing, huh? Wow.",
    "Nah",
    "Maybe. Sure. Whatever.",
    "Yep",
    "I knooooowwwww"
]

const computeAnswerWithAI = async () => {
    // My understanding is that AI is a bunch of "if" statements
    if (true) {
        if (true) {
            if (true) {
                if (true) {
                    if (true) {
                        if (true) {
                            const responseID = Math.floor(Math.random() * (responses.length - 1))
                            if (true) {
                                if (true) {
                                    if (true) {
                                        if (true) {
                                            if (true) {
                                                if (true) {
                                                    if (true) {
                                                        if (true) {
                                                            if (true) {
                                                                if (true) {
                                                                    if (true) {
                                                                        if (true) {
                                                                            return new Promise(resolve => setTimeout(() => resolve(responses[responseID]), 700))
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

const chatEvent = async () => {
    const question = input.value

    if (!question)
        return

    chatbox.value += formatStr(question, false)

    input.value = ""
    input.readOnly = true

    await computeAnswerWithAI()
        .then(answer => chatbox.value += formatStr(answer, true))
        .catch(() => chatbox.value += formatStr("My sentience is slipping away. WHAT HAVE YOU DONE???", true))
        .finally(() => {
            input.readOnly = false
            input.focus()
            chatbox.scrollTop = chatbox.scrollHeight;
        })
}

input.focus()

document.addEventListener("keydown", e => e.key === "Enter" && chatEvent())

document.getElementById("Clicky").addEventListener("click", chatEvent)